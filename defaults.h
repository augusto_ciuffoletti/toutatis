/*
    syd - synchronizes the local system clock with a remote NTP server
    Copyright (C) 1997  Augusto Ciuffoletti

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Contact address: augusto@di.unipi.it
*/

/* 
   Configuration file for the syd command. The following are default values:
   they can be modified by command line parameters:  the option character
   corresponds to the lower case initial of the default constant (e.g. '-m'
   overrides the definition of MIN). See the man page for further details.
*/

#define HOST	"localhost"	/* This is the address of the NTP server */
#define MIN	0.000	/* the minimum communication delay (secs) */
#define EPSILON	0.500	/* the threshold for contact reached (secs) */
#define DELTA	1.000	/* the threshold for contact lost (secs) */
#define W	60.00	/* the lapse between two successive retries (secs) */
#define K	10	/* the number of retries */
#define RHO	0.0002	/* the maximum drift (secs per secs) */
#define IFCFILE	"lc.syd"

/* #define MODE	ADJUST_CLOCK or ADJUST_CLOCK or SET_CLOCK */
