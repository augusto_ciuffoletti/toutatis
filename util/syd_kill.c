#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>
#include <tvops.h>
#include <logck.h>

int
main (int argc, char *argv[])
  {
    struct log_clock *lc;
    if (argc != 2)
      {
         printf("Usage: syd_date <file>\n");
         return -1;
      }
    lc=new_log_clock(0);
    if ( read_log_clock(lc,argv[1]) )
      return -1;
    kill(get_owner(lc),SIGINT);
    return 0;
  }
