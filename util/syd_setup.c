#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>
#include <tvops.h>
#include <logck.h>

int
main (int argc, char *argv[])
  {
    struct log_clock *lc;
    struct timeval tv,prec;
    
    if (argc != 2)
      {
         printf("Usage: syd_setup <file>\n");
         return -1;
      }
    lc=new_log_clock(0);
    if ( read_log_clock(lc,argv[1]) != 0 )
      return -1;
    printf("WARNING\n");
    printf("  This operation is harmful for many activities in your system.\n");
    printf("  Timestamps associated to files may become inconsistent, and\n");
    printf("    timed activities may exhibit random behaviors.\n");
    printf("  The operation of any syd instance would be disrupted by this\n"); 
    printf("    call: the process attached to the interface file will be\n");
    printf("    automatically killed, but it is your responsibility to kill\n");
    printf("    and possibly restart any other instance of syd.\n");
    printf("Advice: it would be a good idea to call this function at startup...\n");
    printf("\nNow press return if you want to proceed, or CTRL-C to interrupt\n");
    getchar();
    get_log_clock(lc,&tv,&prec,NOW);
    if ( settimeofday(&tv, (struct timezone *) NULL) != 0 )
      {
	perror("syd_setup");
        return -1;
      }
    printf("The system clock has been successfully set-up at\n");
    printf("%s (precision %d.%06d secs)\n",
           ctimeval(&tv), prec.tv_sec, prec.tv_usec);
    kill(get_owner(lc),SIGINT);
    return 0;
  }
