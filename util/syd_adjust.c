#include <stdio.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <tvops.h>
#include <logck.h>
#include <sys/timex.h>

int
main (int argc, char *argv[])
  {
    struct log_clock *lc;
    struct timeval offset;
    struct timex tmx;
    double db_offset;
    int usec_offset;
    int k;

    if (argc != 2)
      {
         printf("Usage: syd_date <file>\n");
         return -1;
      }
/* Read the logical clock */
    lc=new_log_clock(0);
    if ( read_log_clock(lc,argv[1]) )
      return -1;
    get_offset(lc, &offset);
    _TVTODB(offset,db_offset);
    usec_offset=( int ) ( db_offset * 1000000 );
/* Test if adjustment is possible for this offset (todo) */

/* Test if past adjustments are amortized */
    tmx.modes=0;
    if ( (k = adjtimex(&tmx)) < 0 )
      {
        perror("syd_adjust");
        return -1;
      }
    if ( tmx.offset != 0 )
      {
        fprintf(stderr,"Sorry, I am still adjusting... \n");
        return -1;
      }
/* Adjust offset */
    tmx.offset= usec_offset;
    tmx.modes=ADJ_OFFSET;
    if ( adjtimex(&tmx) != 0 )
      {
        perror("syd_adjust");
        return -1;
      }
/* debug */
    printf("Now adjusting %f secs\n", db_offset);
    return 0;
  }

