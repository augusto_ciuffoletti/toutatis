# This simple script calibrates the local clock using syd.
# It implements a calibration plan spanning a possibly long time period.

HOST=bernina.ethz.ch  # The NTP server used for obtaining time
BINDIR=.              # The directory containing the syd commands
INTERVAL=2            # Default calibration delay (in days)
ATQUEUE=f             # The queue devoted to clock calibration

# if one parameter is passed, this is the interval to the next calibration
# otherwise the default of 2 days is used
if [ $# -le 0 ]
then
  # warning...
  echo "Calling -calibrate- without parameters resets the previous calibration!"
  echo "Do you really want this? (y/n)"
  read answer
  if [ $answer != "y" ]
  then
    echo "Usage: calibrate <next delay in days>"
    echo "Emergency exit..."
    exit
  fi
  # removes the old adjtime file
  rm /etc/adjtime
  # cancels all jobs on the clock queue
  joblist=`atq -q f | grep "root" | cut -f 4 | tr "\n" " "`  
  atrm $joblist
else
  INTERVAL=$1
fi
# The following tries to obtain an accurate time from the server
# Successive attempts are repeated every 1 minute for 6 times
$BINDIR/syd -h $HOST -w 60 -k 6 &
# Attenzione: se syd fallisce resta bloccato!
while [ ! -e lc_0 ]
do
  sleep 1
done
echo -e "\n***** syd started *****\n"
next_interval=$[ $INTERVAL * 4 ]
hwclock --set --date="`$BINDIR/syd_date lc_0`" --debug
echo -e "\n***** killing syd *****\n"
$BINDIR/syd_kill lc_0
echo "cd ~/clock; $0 $next_interval" |
at -q $ATQUEUE now + $INTERVAL days
