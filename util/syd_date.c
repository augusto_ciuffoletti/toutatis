#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <tvops.h>
#include <logck.h>

int
main (int argc, char *argv[])
  {
    struct log_clock *lc;
    struct timeval time,precision;
    struct timespec ts;

    if (argc != 2)
      {
         printf("Usage: syd_date <file>\n");
         return -1;
      }
    lc=new_log_clock(0);
    if ( read_log_clock(lc,argv[1]) != 0 )
      return -1;
    get_log_clock(lc, &time, &precision, NOW);
    ts.tv_sec=0;
    ts.tv_nsec=(1000000-time.tv_usec)*1000;
    nanosleep(&ts,NULL);
    printf("%s(precision %d.%06d secs)\n",
           ctime((time_t *)&(time.tv_sec)), precision.tv_sec, precision.tv_usec);
    return 0;
  }
