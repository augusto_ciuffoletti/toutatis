#!/bin/sh
if [ $# != 1 ]
then echo "usage: analyze <log-file>" > /dev/stderr; exit -1
fi
echo "This program analyzes the log file -$1- and" 
echo "  extracts some useful informations"
echo "These are the settings for the synchronization session"
grep "|" $1
echo "============="
echo "The epilogue of the session was:"
grep Message $1
echo "============="
echo "Analyzing the drift rate."
echo "  The median of the computed drifts is the following:"
num=`grep Drift $1 | wc -l`
x=$[num/2];
rho=`grep Drift $1 | sort | tail -$x  | head -1`
echo "  -->  $rho"
echo "  This is likely to be the true value of the drift of the local clock"
echo "  A reasonable value for the maximum drift used by the"
echo "  synchronization protocol is two times this figure."
echo "============="
echo "Analyzing the roundtrip."
echo "  This is the minimum roudtrip:"
min=`grep 2D $1 | sort | head -1`
num=`grep 2D $1 | wc -l`
echo "  -->  $min"
echo "  The following are key values of its distribution:"
x=$[(98*num)/100];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  98%:    $val" 
x=$[(19*num)/20];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  95%:    $val" 
x=$[(9*num)/10];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  90%:    $val" 
x=$[(3*num)/4];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  75%:    $val" 
x=$[num/2];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  median: $val" 
x=$[num/4];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  25%:    $val" 
x=$[num/10];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  10%:    $val" 
x=$[num/20];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  5%:     $val" 
x=$[num/50];
val=`grep 2D $1 | sort | tail -$x  | head -1`
echo -e "  1%:     $val" 
echo "============="
echo "Analyzing the number of retries per attempt."
grep retries $1 | sort | uniq -c
num=`grep '(delta_0)' $1 | wc -l`
echo "============="
echo "Analyzing the mode of the retries."
grep "mode" $1 | sort | uniq -c
echo "============="
echo "Analyzing the number of retries that timed out."
num=`grep "timed out" $1 | sort | wc -l`
echo "  --> $num retries timed out"
echo "============="
num=`grep '(delta_0)' $1 | sort | wc -l`
echo "Analyzing the accuracy after an attempt."
echo "  The following are key values of its distribution:"
x=$[(98*num)/100];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  98%:    $val" 
x=$[(19*num)/20];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  95%:    $val" 
x=$[(9*num)/10];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  90%:    $val" 
x=$[(3*num)/4];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  75%:    $val" 
x=$[num/2];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  median: $val" 
x=$[num/4];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  25%:    $val" 
x=$[num/10];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  10%:    $val" 
x=$[num/20];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  5%:     $val" 
x=$[num/100];
val=`grep '(delta_0)' $1 | sort | tail -$x  | head -1`
echo -e "  1%:     $val" 
echo "============="
num=`grep 'Remote accuracy' $1 | wc -l`
echo "Analyzing the accuracy of the server."
echo "  The minimum accuracy:"
grep 'Remote accuracy' $1 | sort | tail -1
echo "  The maximum accuracy:"
grep 'Remote accuracy' $1 | sort | head -1
echo "  The mediam of the accuracy:"
x=$[num/2];
grep 'Remote accuracy' $1 | sort | tail -$x  | head -1 
echo "============="
num=`grep "dna" $1 | sort | wc -l`
echo "Analyzing the dna."
echo "  The minimum dna:"
grep 'dna' $1 | sort | tail -1
echo "  The maximum dna:"
grep 'dna' $1 | sort | head -1
echo "  The mediam of the dna:"
x=$[num/2];
grep 'dna' $1 | sort | tail -$x  | head -1 
