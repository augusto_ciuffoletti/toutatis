#include <stdio.h>
#include <time.h>
#include <string.h>
#include <linux/time.h>

/*
   Generates a printable string representing a timeval.
 */

char *
ctimeval (struct timeval *tv)
{
  static char buffer[80];
  char *eol;

  if ((tv->tv_sec == 0) && (tv->tv_usec == 0))
    {
      strcpy (buffer, "Null");
      return "Null";
    }
  sprintf (buffer, "%s", ctime ((time_t *) & tv->tv_sec));
  eol = buffer + (strlen (buffer) - 1);
  sprintf (eol, " %7.3f msecs", ((double) tv->tv_usec / 1000));
  return buffer;
}
