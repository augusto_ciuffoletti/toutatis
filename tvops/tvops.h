/* 
  $Log: tvops.h,v $
  Revision 1.3  1998/01/23 10:37:35  augusto
  added ctimeval (moved from elsewhere)

  Revision 1.2  1997/11/20 19:00:31  augusto
  First revision tested with Syd v. 3.1

  Revision 1.1  1997/11/18 13:16:28  augusto
  Initial revision
 
*/

#define _RESETTV(X) {X.tv_sec =0; X.tv_usec =0;}
#define _COPYTV(X,Y) {Y.tv_sec = X.tv_sec; Y.tv_usec = X.tv_usec;}
#define _ADDTV(X,Y) {X.tv_sec += Y.tv_sec; X.tv_usec += Y.tv_usec;\
        if (X.tv_usec > 1000000) { X.tv_sec++; X.tv_usec -= 1000000;}}
#define _DIFFTV(X,Y) {X.tv_sec -= Y.tv_sec; X.tv_usec -= Y.tv_usec;\
        if (X.tv_usec < 0) { X.tv_sec--; X.tv_usec += 1000000;}}
#define _TVTODB(X,Y) {Y = ( (double) X.tv_sec ) + \
                          ( (double) X.tv_usec / 1000000 ); }
#define _DBTOTV(X,Y) { { double x; \
                         Y.tv_usec = (long int) (1000000 * modf (X, &x)); \
                         Y.tv_sec = (long int) x; } }
#define NOW (struct timeval *)NULL

char *ctimeval (struct timeval *tv);

