# $Log$

export PACKAGE=toutatis
export NTP_PORT=123
export LOCAL_PORT=1201
export INSTALL_DIR=$(wildcard ~/bin)
export TARFILE=$(PACKAGE).tar
# in the following definition the order matters
DIRS=man logck ntp timeout tvops syd util
ADDENDA=README.install Version Makefile make.common make.init defaults.h

all:
	for f in $(DIRS); do cd $$f; $(MAKE); cd ..; done

local:
	export NTP_PORT=$(LOCAL_PORT); \
         for f in $(DIRS); \
         do \
             cd $$f; \
             $(MAKE); \
             cd ..; \
         done

install:
	$(MAKE) all
	cd syd; $(MAKE) install
	cd util; $(MAKE) install
	cd man; $(MAKE) install
	cd logck; $(MAKE) install

uninstall:
	cd syd; $(MAKE) uninstall
	cd util; $(MAKE) uninstall
	cd logck; $(MAKE) install

clean:
	for f in $(DIRS); do cd $$f; $(MAKE) clean; cd ..; done
compress:
	for f in $(DIRS); do cd $$f; $(MAKE) compress; cd ..; done
uncompress:
	for f in $(DIRS); do cd $$f; $(MAKE) uncompress; cd ..; done
checkpoint:
	$(MAKE) clean
	for dir in $(DIRS); do cd $$dir; $(MAKE) checkpoint; cd ..; done 
tar:
	for dir in $(DIRS); do cd $$dir; $(MAKE) tar; cd ..; done
	for file in $(ADDENDA); do \
          cd ..; \
          tar -rvf $(TARFILE) $(PACKAGE)/$$file; \
          cd $(PACKAGE); \
        done
	gzip -f ../$(TARFILE)
version: Version
	$(MAKE) checkpoint
	$(MAKE) tar
	ci Version
	echo `rlog -h Version | grep head | tr -cd '[:digit:]'` > ../repository/last;\
	cd ../repository; \
           mv --interactive \
              ../$(TARFILE).gz \
              $(PACKAGE)`cat last`.tgz
