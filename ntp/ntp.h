/*
    syd - synchronizes the local system clock with a remote NTP server
    Copyright (C) 1997  Augusto Ciuffoletti

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Contact address: augusto@di.unipi.it
*/

/*
   $Log: ntp.h,v $
   Revision 2.2  1997/12/03 17:31:12  augusto
   The ctimeval function is moved in the appropriate module

   Revision 2.1  1997/11/20 18:38:44  augusto
   First revision tested with Sid v.2.1

   Revision 1.0.1.3  1997/10/31 18:38:12  augusto
   Revision tested with a local server

   Revision 1.0.1.1  1997/10/24 15:23:46  augusto
   Extensive modification, mainly to hide the implementation
   of the NTPpacket (remindful of Java...). The old fp to timeval
   and viceversa have been replaced by simpler ones, that do not
   make use of conversion tables. A test program has been written
   to test most of the features of the module, and in particular
   the encoding and decoding of timestamps.
   Neew functions have been added to the original module to allow
   the manipulation of the packet without addressing to the fields

   Revision 1.0  1997/10/23 14:54:24  augusto
   Initial revision

 */

/* GEN_1970 = (1970 - 1900) in secondi.
 * UNIX misura il tempo a partire dal 1970,  
 * UTC a partire dal 1900   */
#define GEN_1970 2208988800U
#define MODE_CLIENT  3
#define MODE_SERVER  4
#define LEAP_NOTINSYNC 0x3
#define NTP_VERSION ((unsigned char)4)
#define NTP_MINPOLL 6
/*
Moved to the command line (see Makefile)
#define NTP_PORT 123
#define NTP_LOCALPORT 1201
*/
struct pacchettoNTP *new_packet( \
         unsigned char li, \
         int vn, \
         int mode, \
         unsigned char stratum, \
         int poll, \
         int precision);

/* obtains the size of the structure */
int get_size(struct pacchettoNTP *p);
/* pretty prints the content of a message */
void print_NTPpacket (FILE * f, struct pacchettoNTP *p);
/* update the time fields in the packet */
void set_orgtime(struct timeval *tv_in, struct pacchettoNTP *p);
void set_rectime(struct timeval *tv_in, struct pacchettoNTP *p);
void set_xmttime(struct timeval *tv_in, struct pacchettoNTP *p);
/* retrieve the time fields in the packet */
void get_orgtime(struct timeval *tv_out, struct pacchettoNTP *p);
void get_rectime(struct timeval *tv_out, struct pacchettoNTP *p);
void get_xmttime(struct timeval *tv_out, struct pacchettoNTP *p);
/* records the syncronization dispersion in the packet */
void set_dispersion (struct timeval *tv_in, struct pacchettoNTP *p);
/* retrieve the syncronization dispersion in the packet */
void get_dispersion(struct timeval *tv_out, struct pacchettoNTP *p);
/* check if timestamp is correct in the reply */
int check_timestamp(struct pacchettoNTP *reply, \
                    struct pacchettoNTP *sent);
/* 
  dumps the content of the packet in Hex and Decimal. 
   For debuggers only
 */
void dump_NTPpacket(struct pacchettoNTP *p);
