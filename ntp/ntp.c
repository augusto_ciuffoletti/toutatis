/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: ntp.c,v $
   Revision 2.2  1997/12/03 17:31:12  augusto
   The ctimeval function is moved in the appropriate module

   Revision 2.1  1997/11/20 18:38:44  augusto
   First revision tested with Sid v.2.1

   Revision 1.0.1.4  1997/11/06 20:42:02  augusto
   Adapted to the java_syd logging style

   Revision 1.0.1.3  1997/10/31 18:38:12  augusto
   Revision tested with a local server

   Revision 1.0.1.2  1997/10/31 11:46:19  augusto
   First tested revision

   Revision 1.0.1.1  1997/10/24 15:23:46  augusto
   Extensive modification, mainly to hide the implementation
   of the NTPpacket (remindful of Java...). The old fp to timeval
   and viceversa have been replaced by simpler ones, that do not
   make use of conversion tables. A test program has been written
   to test most of the features of the module, and in particular
   the encoding and decoding of timestamps.
   Neew functions have been added to the original module to allow
   the manipulation of the packet without addressing to the fields

   Revision 1.0  1997/10/23 14:53:34  augusto
   Initial revision

 */

#include <stdio.h>
#include <stdlib.h>		/* for malloc */
#include <netinet/in.h>		/* for ntohl, ltonh */
#include <sys/time.h>		/* per struct timeval */
#include <math.h>		/* per struct modf */
#include <limits.h>		/* per USHRT_MAX */
#include "ntp.h"
#include "tvops.h"

/* Formato in virgola fissa usato da NTP per rappresentare i timestamp
 * nei pacchetti. Viene applicato a tali timestamp contenuti nel
 * pacchetto la conversione in Network Byte Order quando vengono
 * trasmessi e la trasformazione inversa ,Local Host Byte Order, 
 * al momento della ricezione  */

typedef struct
  {
    unsigned int l_ui;
    unsigned int l_uf;
  }
fp;


/* struttura del pacchetto NTP .
 * Tutti i valori fp arrivano in Network Byte Order e quindi 
 * vanno convertiti opportunamente */
struct pacchettoNTP
  {
    unsigned char li_vn_mode;
    unsigned char stratum;
    unsigned char ppoll;
    signed char precision;
    int rootdelay;
    unsigned int rootdispersion;
    unsigned int refid;
    fp reftime;
    fp org;			/* timestamp originario */
    fp rec;			/* timestamp di ricezione */
    fp xmt;			/* timestamp di trasmissione */
  };

char * cNTPtime (fp * par_fp);
/*
   Generates a printable string representing a time as 
   represented as a fp inside a message. Also this one 
   is hidden.
 */


/* 

   #include "tvtots.c"
   #include "tstotv.c"

   ======================================
   THE FOLLOWING PART SHOULD BE CAREFULLY
   REWRITTEN (and it was...)
   ======================================

   void ConvertiTV_NTP (struct timeval t, fp * timestamp);
   void ConvertiNTP_TV (fp tempo_trasm_Server, struct timeval *t);

void
ConvertiTV_NTP (t, timestamp)

     struct timeval t;
     fp *timestamp;

{
  if (t.tv_usec >= 1000000)
    {
      t.tv_usec -= 1000000;
      t.tv_sec++;
    }
  timestamp->l_ui = t.tv_sec + GEN_1970;
  timestamp->l_uf = (ustotslo[t.tv_usec & 0xff] +
		     ustotsmid[(t.tv_usec >> 8) & 0xff] +
		     ustotshi[(t.tv_usec >> 16) & 0xf] + 0x00000800) &
    0xfffff000;

}

void
ConvertiNTP_TV (tempo_trasm_Server, t)

     fp tempo_trasm_Server;
     struct timeval *t;

{
  unsigned int x;

  x = tempo_trasm_Server.l_uf;
  t->tv_sec = tempo_trasm_Server.l_ui - GEN_1970;
  t->tv_usec = (tstoushi[(x >> 24) & 0xff] + tstousmid[(x >> 16) &
			      0xff] + tstouslo[(x >> 9) & 0x7f] + 0x4) >> 3;
  if (t->tv_usec == 1000000)
    {
      t->tv_sec++;
      t->tv_usec = 0;
    }
}

== == == == == == == == == == == == == == == =
END OF THE PART TO BE REWRITTEN
== == == == == == == == == == == == == == == =
*/

/*
   The following are two functions that convert the NTP
   fixed point notation into Unix BSD timeval and viceversa.
   Their scope is limited to the ntp module, since other
   modules cannot access to the internal structure of the
   NTP packet.
 */

static void tv2fp (struct timeval t, fp * timestamp);
static void fp2tv (fp timestamp, struct timeval *t);

void 
tv2fp (struct timeval t, fp * timestamp)
{
  (timestamp->l_ui) = t.tv_sec+(GEN_1970);
  (timestamp->l_uf) = t.tv_usec * (UINT_MAX / 1000000.0);
}

void 
fp2tv (fp timestamp, struct timeval *t)
{
  t->tv_sec = timestamp.l_ui-(GEN_1970);
  t->tv_usec = timestamp.l_uf / (UINT_MAX / 1000000.0);

}

char *
cNTPtime (fp * par_fp)
{
  struct timeval tmp_tv;	/* temporaneo per le conversioni */
  fp tmp_fp;			/* temporaneo per le conversioni */

  tmp_fp.l_ui = ntohl (par_fp->l_ui);
  tmp_fp.l_uf = ntohl (par_fp->l_uf);
  fp2tv (tmp_fp, &tmp_tv);
  return ctimeval (&tmp_tv);
}

struct pacchettoNTP *
new_packet (\
	    unsigned char li, \
	    int vn, \
	    int mode, \
	    unsigned char stratum, \
	    int poll, \
	    int precision)
{
  struct pacchettoNTP *pacchetto;

  pacchetto = (struct pacchettoNTP *) malloc (sizeof (struct pacchettoNTP));
  pacchetto->li_vn_mode = \
    (unsigned char) ((li << 6) | \
		     (vn << 3) | \
		     (mode));
  pacchetto->stratum = stratum;
  pacchetto->ppoll = poll;
  pacchetto->precision = precision;
  pacchetto->rootdelay = 0;
  pacchetto->rootdispersion = 0;
  pacchetto->refid = 0;
  pacchetto->reftime.l_ui = 0;
  pacchetto->reftime.l_uf = 0;
  pacchetto->org.l_ui = 0;
  pacchetto->org.l_uf = 0;
  pacchetto->rec.l_ui = 0;
  pacchetto->rec.l_uf = 0;
  return pacchetto;
}

/* obtains the size of the structure */
int 
get_size (struct pacchettoNTP *p)
{
  return sizeof (struct pacchettoNTP);
}

/*
   Printf the content of a NTPpacket on a stream
 */
void
print_NTPpacket (FILE * f, struct pacchettoNTP *p)
{
  fprintf (f, "Flags\tStratum\tPoll\tPrec.\tR_Del.\tR_Disp.\tRef. Id\n");
  fprintf (f, "%o\t%o\t%o\t%d\t", p->li_vn_mode, p->stratum, p->ppoll, p->precision);
  fprintf (f, "%d\t%u\t%u\t\n", p->rootdelay, p->rootdispersion, p->refid);
  fprintf (f, "reftime: \t%s\n", cNTPtime (&(p->reftime)));
  fprintf (f, "originate: \t%s\n", cNTPtime (&(p->org)));
  fprintf (f, "receive: \t%s\n", cNTPtime (&(p->rec)));
  fprintf (f, "transmit: \t%s\n", cNTPtime (&(p->xmt)));
}

/*
   The following functions update the time fields of the message
 */

void 
set_orgtime (struct timeval *tv_in, struct pacchettoNTP *p)
{
  fp tmp_fp;

  tv2fp (*tv_in, &tmp_fp);
  (p->org).l_ui = htonl (tmp_fp.l_ui);
  (p->org).l_uf = htonl (tmp_fp.l_uf);
  return;
}
void 
set_rectime (struct timeval *tv_in, struct pacchettoNTP *p)
{
  fp tmp_fp;

  tv2fp (*tv_in, &tmp_fp);
  (p->rec).l_ui = htonl (tmp_fp.l_ui);
  (p->rec).l_uf = htonl (tmp_fp.l_uf);
  return;
}
void 
set_xmttime (struct timeval *tv_in, struct pacchettoNTP *p)
{
  fp tmp_fp;

  tv2fp (*tv_in, &tmp_fp);
  (p->xmt).l_ui = htonl (tmp_fp.l_ui);
  (p->xmt).l_uf = htonl (tmp_fp.l_uf);
  return;
}

/*
   The following functions retrieve the time fields in a packet
 */

void 
get_orgtime (struct timeval *tv_out, struct pacchettoNTP *p)
{
  fp tmp_fp;

  tmp_fp.l_ui = ntohl ((p->org).l_ui);
  tmp_fp.l_uf = ntohl ((p->org).l_uf);
  fp2tv (tmp_fp, tv_out);
  return;
}

void 
get_rectime (struct timeval *tv_out, struct pacchettoNTP *p)
{
  fp tmp_fp;

  tmp_fp.l_ui = ntohl ((p->rec).l_ui);
  tmp_fp.l_uf = ntohl ((p->rec).l_uf);
  fp2tv (tmp_fp, tv_out);
  return;
}

void 
get_xmttime (struct timeval *tv_out, struct pacchettoNTP *p)
{
  fp tmp_fp;

  tmp_fp.l_ui = ntohl ((p->xmt).l_ui);
  tmp_fp.l_uf = ntohl ((p->xmt).l_uf);
  fp2tv (tmp_fp, tv_out);
  return;
}

/* 
   Return the synchronization dispersion. This is represented
   in the packet as an unsigned integer (32 bits), whose
   first 16 bits (0-15) represent seconds, and the remaining
   16 bits (16-31) represent the fractional part.
   The result of the division of the whole integer by USHRT_MAX
   (that corresponds to 2^16)
   is assigned to the "ui" part of the result timeval, while the
   rest of the same division is assigned to the "uf" part.
 */

void 
get_dispersion (struct timeval *tv_out, struct pacchettoNTP *p)
{
  int tmp;

  tmp = ntohl (p->rootdispersion);
  (tv_out->tv_sec) = (int) (tmp / USHRT_MAX);
  (tv_out->tv_usec) = (int) \
    (tmp % USHRT_MAX) * (1000000.0 / USHRT_MAX);
  return;
}

void 
set_dispersion (struct timeval *tv_in, struct pacchettoNTP *p)
{
  int tmp;

  tmp = (tv_in->tv_sec) * USHRT_MAX;
  tmp += (tv_in->tv_usec) / (1000000.0 / USHRT_MAX);
  (p->rootdispersion) = htonl (tmp);
  return;
}

/*
   The following function compares the org field of a packet with the
   xmt field of another, to check if the former is a reply to the
   latter.
 */

int 
check_timestamp (struct pacchettoNTP *reply, \
		 struct pacchettoNTP *sent)
{
  if ((((reply->org).l_ui) == ((sent->xmt).l_ui)) &&
      (((reply->org).l_uf) == ((sent->xmt).l_uf)))
    return 1;
  else
    return 0;
}

void 
dump_NTPpacket (struct pacchettoNTP *p)
{
  int *pt = (int *) p;
  int i;

  printf ("Packet content. Each line 32 bits (4 bytes)\n====\noffset hex        decimal\n");
  for (i = 0; i < 12; i++)
    {
      printf ("%02d     %#010x %d\n", i, *(pt), *(pt));
      pt++;
    }
  printf ("====\n");
  return;
}
