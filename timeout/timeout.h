/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: timeout.h,v $
   Revision 1.1  1997/11/20 18:48:46  augusto
   Initial revision

 */

void null_timeout();	/* sets a null timeout */
int wait_timeout();	/* waits until the timeout expires */
void cancel_timeout();	/* cancels a pending timeout (if any) */
/* sets a timeout */
int set_timeout (struct timeval *Dx, struct timeval *when);
/* computes the time left time to a given deadline */
int get_deadline (struct timeval *t, struct timeval *Dx, \
                  struct timeval *when);

