/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: timeout.c,v $
   Revision 1.1  1997/11/20 18:48:46  augusto
   Initial revision

 */
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>
#include <tvops.h>
#include <asm-generic/signal-defs.h>
#include "timeout.h"

static void alarm_handler ();

/*
  The timeout flag is 
  1 - when there is no active timeout (i.e. the last one already expired) 
  0 - when there is a timeout pending.
 */
int timeout_flag=1;

void
cancel_timeout()
{
  alarm(0);
  timeout_flag=1;
}

int
set_timeout (struct timeval *Dx, struct timeval *when)
{
  struct timeval tmp_tv;
  struct itimerval D;

/* assigns a handler to the timeout signal */
  signal (SIGALRM, alarm_handler);
  _RESETTV ((D.it_interval));
  _COPYTV ((*Dx), (D.it_value));
  if (when != NOW)
    {
      gettimeofday (&tmp_tv, (struct timezone *) NULL);
      _DIFFTV (tmp_tv, (*when));
      _DIFFTV (D.it_value, tmp_tv);
      if (D.it_value.tv_sec < 0)    /* deadline is past present time */ 
	{
	timeout_flag=1;
        return -1;  
	}
    }
  setitimer (ITIMER_REAL, &D, (struct itimerval *) NULL);
  timeout_flag=0;  
  return 0;
}

int
wait_timeout()
{
  sigset_t block_timeout;	/* mask to block the timeout signal */
  sigset_t dont_block;		/* empty mask */

/* if timeout already over return immediately with -1 */
  if (timeout_flag != 0)
    return -1;
/* initialize the basic empty signal mask */
  sigemptyset (&dont_block);
/* initialize a mask to reliably read the timeout_flag */
  sigemptyset (&block_timeout);
  sigaddset (&block_timeout, SIGALRM);
/* 
   Reliably waits until the deadline expires, while handling signals
   See GNU C library, sect. 21.8.3 
 */
  sigprocmask (SIG_BLOCK, &block_timeout, (sigset_t *) NULL);
  while (timeout_flag != 1)
    sigsuspend (&dont_block);
  sigprocmask (SIG_UNBLOCK, &block_timeout, (sigset_t *) NULL);
  return 0;
}

int
get_deadline (struct timeval *t,struct timeval *Dx, struct timeval *when)
{
  struct timeval tmp_tv;

  _COPYTV ((*Dx), (*t));
  if (when != NOW)
    {
      gettimeofday (&tmp_tv, (struct timezone *) NULL);
      _DIFFTV (tmp_tv, (*when));
      _DIFFTV ((*t), tmp_tv);
      if (t->tv_sec < 0)
        return -1;
    }
  return 0;
}

void
alarm_handler ()
{
  timeout_flag = 1;
}
