/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: retry.c,v $
   Revision 3.5  1998/01/16 13:41:36  augusto
   Terminology: precision changed to accuracy everywhere

   Revision 3.4  1998/01/12 08:33:53  augusto
   "This code should disappear" part eliminated!
   Added inconsistent timing detection and notification
   Some code common to the bound clock and the unbound clock parts
     has been duplicated and the two parts are made self contained.

   Revision 3.3  1997/12/04 11:40:53  augusto
   IMPORTANT REVISION
   A bug has been fixed in the evaluation of USELESS attempts.
   The previous criteria to discriminate a CRISTIAN from a USELESS
   attempt considered the accuracy of the clock reading attempt,
   without taking into account the server's accuracy (dispersion),
   but compared it with the current logical clock accuracy, which
   was reminiscent of the accuracy of the previous synchronization,
   which embedded the server's accuracy. When the server accuracy
   was low, compared with the transmission delay, the new reading
   was always considered as non USELESS, even if the new accuracy
   (including the server's dispersion) was worse than the previous.

   This bug led to some unexpected "contact lost" conditions in case of
   dramatic loss of accuracy in the server's time service.

   Revision 3.2  1997/12/03 17:34:02  augusto
   Updated according to a mimor change in get_offset and get_drift

   Revision 3.1  1997/11/20 17:46:35  augusto
   This is the first version correctly running on the Internet
   Tested locally using test1 through test10 and on the
   Internet with NTP server tempo.cstv.to.cnr.it

 */

#include <stdio.h>
#include <sys/time.h>		/* for struct timeval */
#include <sys/types.h>		/* for select timeval */
#include <math.h>		/* for function fabs() */
/* The following are Syd related headers */
#include <ntp.h>
#include <logck.h>
#include <tvops.h>
#include "log.h"
#include "syd.h"

static void fill_retry_report (char b[]);

int
retry ()
/* =================================================
   This function applies the reading rule, that calculates an approximation
   of UTC time based on the timing of the remote clock reading (contained in
   the structure "attempt_summary") and of the previous accuracy of the
   logical clock LC.  The new approximation is used to update the logical
   clock.

   The function returns the same code returned by read_remote_clock:
   0 if the attempt performed correctly, 1 if a timeout occurred during the
   attempt (the deadline is W secs fromt the beginning of the attempt),
   and -1 if a socket related error occurred (connection refused or
   such). In the last case the variables errno and herrno are set
   accordingly.

   The function fills-in the report if and only if it does not terminate
   with a fatal error.

   The code is strictly adherent to that presented in the paper, and it is
   not annotated.  
   ================================================= */

{
  double t0, t1, tt, tr;	/* timing of the clock reading attempt */
  double l1, l2;		/* threshold values */
  double x, tmp_db;		/* temporary doubles */
  struct timeval tmp_tv;	/* temporary timeval */
  struct timeval t;		/* new clock value */
  struct timeval delta_0_tv;	/* delta_0 (local clock accuracy) 
				   as a timeval */
  double delta_0;		/* delta_0 as a double */
  int s;			/* return status from the clock reading */
  char buffer[2000];		/* report buffer */

/* Filling the report... */
  stamp_log ();
  write_log ("    New retry\n");
  if (get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW) != 0)
    write_log ("    Initial accuracy is unbound\n");
  else
    {
      sprintf (buffer, "    Initial accuracy is        %6.2f msecs\n",
	       (delta_0_tv.tv_usec / 1000.0) + (delta_0_tv.tv_sec * 1000));
      write_log (buffer);
    }
/* reads the remote clock */
  if ((s = read_remote_clock ()) != 0)
    {
      if (s == 1)
	write_log ("    The retry timed out\n");
      flush_log ();
      return s;
    }

/* The following convert timevals to doubles */
  _TVTODB (attempt_summary->tr, tr)
    _TVTODB (attempt_summary->tt, tt)
    _TVTODB (attempt_summary->t0, t0)
    _TVTODB (attempt_summary->t1, t1)

    attempt_summary->D = ((t1 - t0) - (tt - tr)) / 2;
  if ( get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW) != 0 )
    /* clock accuracy is un-bound */
    {
      attempt_summary->psi = attempt_summary->D - input_data->min;
/* Compute the approximation of the server time at t1 */
      tmp_db = (tt + input_data->min + attempt_summary->psi);
      _DBTOTV (tmp_db, t)
/* Compute the upper bound of the clock dispersion */
      _TVTODB (attempt_summary->server_dispersion, tmp_db)
      delta_0 = (attempt_summary->psi) + tmp_db;
      _DBTOTV (delta_0, delta_0_tv)
      attempt_summary->mode = INITIAL;
/* The accuracy of the clock is bound */
      set_log_clock (LC, &t, &delta_0_tv, &(attempt_summary->receive));
    }
  else
    /* clock accuracy is bound */
    {
      double delta_sum;

      _TVTODB (delta_0_tv, delta_0)
      _TVTODB (attempt_summary->server_dispersion, delta_sum)
      delta_sum += delta_0;
      /* delta_sum is the dispersion of the relative timings */
      if ( (attempt_summary->alpha_max = tr - t0 +
            delta_sum - input_data->min ) < 0)
        {
	  serrno = INCONSISTENT_TIMING;
          return -1;
	}
      attempt_summary->alpha_min = tr - t0 - delta_sum - input_data->min;
      if (attempt_summary->alpha_min < 0)
	attempt_summary->alpha_min = 0;
      attempt_summary->beta_min = \
	2 * attempt_summary->D - 2 * input_data->min - attempt_summary->alpha_max;
      if (attempt_summary->beta_min < 0)
	attempt_summary->beta_min = 0;
      if ( ( attempt_summary->beta_max =
	     2 * attempt_summary->D - 2 * input_data->min -
             attempt_summary->alpha_min ) < 0)
        {
	  serrno = INCONSISTENT_TIMING;
          return -1;
	}
      x = fabs (attempt_summary->D - (input_data->min + delta_sum));
      l1 = t0 + attempt_summary->D - x;
      l2 = t0 + attempt_summary->D + x;
      if ((tr < l1) || (tr > l2))
	{
	  attempt_summary->mode = ALA_CIU;
	  if (attempt_summary->alpha_max < attempt_summary->beta_max)
	    attempt_summary->psi = attempt_summary->alpha_max / 2;
	  else
	    attempt_summary->psi = attempt_summary->beta_max / 2;
	}
      else
	{
	  attempt_summary->mode = CRISTIAN;
	  attempt_summary->psi = attempt_summary->D - input_data->min;
	}
/* Compute the approximation of the server time at t1 */
      tmp_db = (tt + input_data->min + attempt_summary->beta_min + attempt_summary->psi);
      _DBTOTV (tmp_db, t)
/* Check if the new accuracy is better than the former 
   and update the logical clock only if so */
      _TVTODB (attempt_summary->server_dispersion, tmp_db)
      if ( delta_0 > (attempt_summary->psi) + tmp_db )
        {
	  delta_0 = (attempt_summary->psi) + tmp_db;
          _DBTOTV (delta_0, delta_0_tv)
	  set_log_clock (LC, &t, &delta_0_tv, &(attempt_summary->receive));
        }
      else
        {
	  attempt_summary->mode = USELESS;
        }
    }

/* Filling the report... */
  fill_retry_report (buffer);
  write_log (buffer);
  flush_log ();
/* and returns */
  return 0;
}

void
fill_retry_report (char b[])
{
  struct timeval tmp_tv,offset;
  struct timeval delta_0_tv;
  char *mode;
  int drift;

  b[0] = '\0';
  sprintf (b + strlen (b), "    Remote clock reading report (session 0):\n");
  sprintf (b + strlen (b), "    Local transmit time (t0):   %s\n",
	   ctimeval (&(attempt_summary->t0)));
  get_rectime (&tmp_tv, received);
  sprintf (b + strlen (b), "    Remote receive time (tr):   %s\n",
	   ctimeval (&(tmp_tv)));
  get_rectime (&tmp_tv, received);
  sprintf (b + strlen (b), "    Remote transmit time (tx):  %s\n",
	   ctimeval (&(tmp_tv)));
  sprintf (b + strlen (b), "    Local receive time (t1):    %s\n",
	   ctimeval (&(attempt_summary->t1)));
  sprintf (b + strlen (b), "    Remote accuracy:           %6.2f msecs\n",
	   ((attempt_summary->server_dispersion).tv_usec / 1000.0) +
	   ((attempt_summary->server_dispersion).tv_sec * 1000));
  sprintf (b + strlen (b), "    Compensated roundtrip (2D): %6.2f msecs\n",
	   attempt_summary->D * 2000);
  switch (attempt_summary->mode)
    {
    case INITIAL:
      mode = "INITIAL";
      break;
    case USELESS:
      mode = "USELESS";
      break;
    case CRISTIAN:
      mode = "CRISTIAN";
      break;
    case ALA_CIU:
      mode = "ALARI_CIUFFOLETTI";
      break;
    default:
      mode = "NOT_SYNC";
      break;
    }
  sprintf (b + strlen (b), "    Retry mode was:             %s\n", mode);
  if (get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW) == 0)
    {
      sprintf (b + strlen (b), "    New accuracy (delta_0):    %6.2f msecs",
	       (delta_0_tv.tv_usec / 1000.0) + (delta_0_tv.tv_sec * 1000));
      sprintf (b + strlen (b), " [%4.2f,%4.2f]\n",
	       input_data->epsilon * 1000, input_data->delta_max * 1000);
      get_offset(LC,&offset);
      sprintf (b + strlen (b), "    Offset:                      %6.2f msecs\n",
	       (offset.tv_usec / 1000.0) + (offset.tv_sec * 1000));     
      if (get_drift(LC,&drift) != -1)
        sprintf (b + strlen (b), "    Drift:                       %6d ppm\n",
	       drift);     
      sprintf (b + strlen (b), "\n");
    }
  else
    {
      sprintf (b + strlen (b), "    Final accuracy is unbound\n");
      sprintf (b + strlen (b), "\n");
    }
  return;
}
