/*
    syd - synchronizes the local system clock with a remote NTP server
    Copyright (C) 1997  Augusto Ciuffoletti

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Contact address: augusto@di.unipi.it
*/

/*
   $Log: syd.h,v $
   Revision 3.5  1998/01/16 13:35:07  augusto
   fixed time units in the comments etc.

   Revision 3.4  1998/01/12 08:33:53  augusto
   Added error code for inconsistent timing
   Removed "w" from input data

   Revision 3.3  1997/12/03 17:34:02  augusto
   Updated to use the interface file

   Revision 3.2  1997/11/27 08:17:38  augusto
   rho_ppm deleted from input_data

   Revision 3.1  1997/11/20 17:46:35  augusto
   This is the first version correctly running on the Internet
   Tested locally using test1 through test10 and on the
   Internet with NTP server tempo.cstv.to.cnr.it

   Revision 1.0  1997/11/18 09:59:05  augusto
   Initial revision

   Revision 1.1  1997/11/18 09:56:25  augusto
   Initial revision

*/

/* Synchronization modes (see struct a_s)*/
#define INITIAL  0  /* the clock was unreliable */
#define USELESS  1  /* the reading prec. was lower than the current prec. */
#define CRISTIAN 2  /* the plain Cristian's rule was applied */
#define ALA_CIU  3  /* the Alari-Ciuffoletti rule was applied */
#define NOT_SYNC 4  /* will become obsolete */

/* Synchronization modes (see struct i_d) */
#define LOGICAL_CLOCK     0 /* the program maintains a logical clock without
                               affecting the system clock */
#define SET_CLOCK	  1 /* the clock is set up without amortization */
#define ADJUST_CLOCK      2 /* the clock change is amortized */

/* Error codes for serrno */
#define SUCCESS		    0
#define BEST_EFFORT         1  /* contact not fully reached */
#define NO_CONTACT          2  /* contact not reached */
#define CONTACT_LOST        3  /* contact lost after commit */
#define INCONSISTENT_DATA   4  /* input line values are inconsistent */
#define LATENT_SYDE         5  /* input line values are inconsistent */
#define LATENT_INETE        6  /* input line values are inconsistent */
#define INCONSISTENT_TIMING 7  /* observed time values are inconsistent
                                  with local estimates */

/* Strutture globali */

/* server description */
struct i_d {
  char *host;	        /* IP address of the server */
  double rho;	        /* upper bound of the drift of the host (ratio) */
  double min;	        /* lower bound of the communication delay (secs) */
  double epsilon;	/* the "contact reached" precision threshold (secs) */
  double delta_max;	/* the worst admitted precision threshold (secs) */
  struct timeval w_tv;	/* the interval between successive attempts */
  int k;	        /* the number of attempts before diagnosing 
                           unreachability */
  char *ifc_file;       /* the name of the interface file */
  int verbose;	        /* if TRUE, be verbose */
  int mode;             /* LOGICAL_CLOCK, or SET_CLOCK, or ADJUST_CLOCK */
};


struct a_s {
/* The "doubles" are expressed in seconds, with 6 decimal digits */
/* Synchro attempt timing */
    struct timeval tr;		/* server time when request is received */
    struct timeval tt;		/* server time when reply is sent */
    struct timeval t0;		/* local time when request is sent */
    struct timeval t1;		/* local time when reply is received */
    struct timeval baseline;	/* the system time at T0 (displacement) */
    struct timeval receive;	/* the system time at T1 (displacement) */
    struct timeval server_dispersion; /* the dispersion of server's clock */
/* The following are computed by the readrule */
    double D;			/* the half roundtrip time */
    double alpha_min, alpha_max;/* Boundaries for the client to server time */
    double beta_min, beta_max;	/* Boundaries for the client to server time */
    double psi;			/* the estimated precision after the synchronization round */
    int mode;     /* This is the operation mode of the readrule :
                     NOT_SYNC = never run; 
                     USELESS, CRISTIAN, ALA_CIU = see paper; */
    int count;    /* records the number of attempts: counts backward from k
                   down to negative values (if precision remains 
                   acceptable) */
};

/* Prototypes of the functions */
void serror(char *);	  /* in the synchro.h file */
/* The following are the four internal loops (from outside) */
int synchro ();           /* in the synchro.c file */
int attempt ();           /* in the attempt.c file */
int retry ();             /* in the retry.c file */
int read_remote_clock (); /* in the lcr.c file */

/* global data */
extern struct i_d *input_data;	     /* synchro session input data */
extern struct a_s *attempt_summary;  /* synchro attempt data base */
extern struct log_clock *LC;	     /* the logical clock */
extern struct pacchettoNTP *received;/* points to the received packet */
extern struct pacchettoNTP *sent;    /* points to the sent packet */

/* Global error indicator */
extern int serrno;
