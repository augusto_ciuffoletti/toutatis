/*
    syd - synchronizes the local system clock with a remote NTP server
    Copyright (C) 1997  Augusto Ciuffoletti

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Contact address: augusto@di.unipi.it
*/

/*
 $Log: syd.c,v $
 Revision 3.5  1998/01/16 13:41:36  augusto
 Fixed an option indicator

 Revision 3.4  1998/01/12 08:33:53  augusto
 Added inconsistent input data detection and notification;
 Added the_end function (diagnostics and interface file removal);

 Revision 3.3  1997/12/03 17:34:02  augusto
 Interruption handler is introduced here
 Updated to use the interface file

 Revision 3.2  1997/11/27 08:17:38  augusto
 added parse_command_line

 Revision 3.1  1997/11/20 17:46:35  augusto
 This is the first version correctly running on the Internet
 Tested locally using test1 through test10 and on the
 Internet with NTP server tempo.cstv.to.cnr.it

 */

#include <stdio.h>
#include <stdlib.h>		/* for malloc */
#include <unistd.h>
#include <getopt.h>
#include <signal.h>		/* for macro SIGALRM */
#include <sys/time.h>		/* for struct timeval */
#include <netdb.h>		/* for herror */
/* These are Syd related headers */
#include <logck.h>
#include <tvops.h>
#include "syd.h"
#include "log.h"

static struct i_d *parse_command_line (int argc, char *argv[]);
static void interruption_handler ();
static void the_end();
static int consistency_check ();
static void fill_syd_header(char * buffer);

/* global data shared by all modules */
struct a_s *attempt_summary;	/* synchro attempt data base */
struct i_d *input_data;		/* synchro session input data */
struct log_clock *LC;		/* the logical clock */
struct pacchettoNTP *received;	/* points to the received packet */
struct pacchettoNTP *sent;	/* points to the sent packet */

/* Error messages for serrno */
char *serrno_messages[] =
{
  "Success",
  "Best effort",
  "No contact",
  "Contact lost",
  "Inconsistent input data",
  "Latent syd error",
  "Latent network error",
  "Inconsistent timing"
};

/* Error code variable shared by all modules */
int serrno;

int
main(int argc, char *argv[])
{
  char buffer[10000];
  int s; /* the exit code */

  signal(SIGINT, interruption_handler);
  atexit(the_end);
/* read default settings and command line parameters */
  input_data = parse_command_line (argc, argv);
  if ( consistency_check() != 0 )
    return -1;
  attempt_summary = (struct a_s *) malloc (sizeof (struct a_s));
/* Creating global data structures */
  LC = new_log_clock (input_data->rho * 1000000);
  stamp_log();
  fill_syd_header(buffer);
  write_log(buffer);
  flush_log();
/* Starting a new run */
  if ((s = synchro ()) != 0)
    {
      fprintf (stderr, "Error detected (synchro returns %d)\n", s);
      return -1;
    }
  fprintf (stderr, "Syd terminated successfully (?!)\n");
  return 0;
}

void
fill_syd_header(char * b)
{
  double w;

  b[0] = '\0';
  sprintf(b + strlen(b),"======== Common header ========\n");
  sprintf(b + strlen(b),"Mode: %s; \t","nil");
  sprintf(b + strlen(b),"rho = %d ppm\n",(int) ((input_data->rho)*1000000));;
  sprintf(b + strlen(b), 
    "0| ======== Synchronization session header ========\n");
  sprintf(b + strlen(b), "0| NTP server %s \t", input_data->host);       
  sprintf(b + strlen(b),"interface file: %s\n",input_data->ifc_file);
  sprintf(b + strlen(b),"0| min = %d msecs \t",(int) ((input_data->min)*1000));       
  sprintf(b + strlen(b),"epsilon = %d msecs \t",(int) ((input_data->epsilon)*1000));     
  sprintf(b + strlen(b),"delta_max = %d msecs\n", (int) ((input_data->delta_max)*1000));
  _TVTODB((input_data->w_tv),w);
  sprintf(b + strlen(b),"0| W = %3.2f secs \t\t", w);
  sprintf(b + strlen(b),"k = %d times\n\n",input_data->k);
}

int
consistency_check ()
{
  double w;
  _TVTODB(input_data->w_tv,w) 
  if (((input_data->delta_max - input_data->epsilon) / input_data->rho - \
       input_data->k * w) < 0)	/* check consistency */
    {
      serrno = INCONSISTENT_DATA;
      return -1;
    }
  return 0;
}

struct i_d *parse_command_line(int argc, char *argv[])
{
  char c;
  double w;           /* temporary location for W */
  struct i_d *result; /* returned struct */
 
  result = (struct i_d *) malloc(sizeof(struct i_d));

/* default values */ 
  result->host = HOST;        /* the address of the host */
  result->min = MIN;
  result->rho = RHO;
  result->epsilon = EPSILON;  /* the desired dispersion after contact */
  result->delta_max = DELTA;  /* the maximum allowed dispersion */ 
  _DBTOTV (W,result->w_tv);    /* the interval between retries */ 
  result->k = K;              /* the number of retries */
  result->ifc_file = IFCFILE;

  while ((c = getopt (argc, argv, "h:m:r:e:d:w:k:f:")) != -1)
    switch (c)
      {
      case 'h':
	result->host = optarg;
	break;
      case 'm':
	result->min = atof (optarg);
	break;
      case 'r':
	result->rho = atof (optarg);
	break;
      case 'e':
	result->epsilon = atof (optarg);
	break;
      case 'd':
	result->delta_max = atof (optarg);
	break;
      case 'w':
	w = atof (optarg);
        _DBTOTV (w,result->w_tv);
	break;
      case 'k':
	result->k = atoi (optarg);
	break;
      case 'f':
	result->ifc_file = optarg;
	break;
      case '?':
	fprintf (stderr, "Unknown option! \n");
	abort ();
      default:
	fprintf (stderr, "Unknown option! \n");
	abort ();
      }
  
  return result;
}


void
serror (char *s)
{
  if (s != NULL)
    fprintf (stderr, "%s: %s\n", s, serrno_messages[serrno]);
  else
    fprintf (stderr, "%s\n", serrno_messages[serrno]);
}

void 
interruption_handler ()
  {
    fprintf(stderr, "\n\n****************************************\n");
    fprintf(stderr, "Syd on %s was interrupted\n", input_data->ifc_file);
    exit(0);
  }

void 
the_end ()
{
  herror ("Message (herror)");
  perror ("Message");
  serror ("Message (serror)");
  remove(input_data->ifc_file);
}
