/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: log.c,v $
   Revision 3.1  1997/11/20 17:46:35  augusto
   This is the first version correctly running on the Internet
   Tested locally using test1 through test10 and on the
   Internet with NTP server tempo.cstv.to.cnr.it

   Revision 1.0.1.1  1997/11/18 09:55:07  augusto
   Prima versione testata e funzionante in rete

   Revision 1.0  1997/11/07 12:41:32  augusto
   Initial revision

 */

#include <stdio.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>

#define BUFLEN 10000		/* 10000 chars is about 5 full pages */

char buffer[BUFLEN] = "";	/* log buffer */

void
stamp_log ()
{
  struct timeval time_tv;

  gettimeofday (&time_tv, NULL);
  sprintf (buffer + strlen (buffer), "Timestamp=%.3f\n",
  ((double) time_tv.tv_sec) * 1000.0 + ((double) time_tv.tv_usec) / 1000.0);
  return;
}

int
write_log (char *s)
{
  int space_left;

  space_left = BUFLEN - strlen (buffer);
  if (strlen (s) > (space_left - 1))
    {
      errno = ENOMEM;
      return -1;
    }
  strcpy (buffer + strlen (buffer), s);
  return 0;
}

int
flush_log ()
{
  fprintf (stderr, "%s", buffer);
  buffer[0] = '\0';
  return 0;
}
