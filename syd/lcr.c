/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: lcr.c,v $
   Revision 3.2  1998/01/16 13:18:45  augusto
   Terminology: precision is changed into accuracy.

   Revision 3.1  1997/11/20 17:46:35  augusto
   This is the first version correctly running on the Internet
   Tested locally using test1 through test10 and on the
   Internet with NTP server tempo.cstv.to.cnr.it

 */

/* 

   The main function is read_remote_clock: it reaches the remote NTP server
   and obtains a fresh clock value. The function also collects the best
   possible timing of the two ways communication (namely t_0 and t_1 in the
   paper).

   If the clock reading operation is successful, the function returns 0 to
   the calling function. If the communication with the server times out (the
   timeout is W secs starting from the attempt baseline) while waiting for
   the reply, the function returns 1. If any of the internet or socket
   related calls fail, the function returns -1.

 */

#include <stdio.h>
#include <stdlib.h>		/* for EXIT codes */
#include <unistd.h>		/* for read and close */
#include <string.h>		/* for memcpy */
#include <sys/socket.h>		/* for socket macros and functions */
#include <netinet/in.h>		/* for socket macros and functions */
#include <arpa/inet.h>		/* for socket macros and functions */
#include <netdb.h>		/* for sockaddr_in */
#include <signal.h>		/* for SIGALRM */
#include <sys/time.h>		/* for struct timeval */
#include <math.h>		/* for function pow */
/* The following are Syd related header files */
#include <ntp.h>
#include <logck.h>
#include <timeout.h>
#include <tvops.h>
#include "syd.h"

static int open_socket (int *s);
static int generate_address (struct sockaddr_in *sa, char *host);

int
read_remote_clock ()
{
  int socket_descr;
  struct sockaddr_in address;
  int packet_length;
  int msg_ok_flag;
  fd_set read_set;
  struct timeval read_timeout;

/* Creates new packets */
  sent = new_packet (\
		     LEAP_NOTINSYNC, \
		     NTP_VERSION, \
		     MODE_CLIENT, \
		     0, \
		     NTP_MINPOLL, \
		     0 \
    );
  received = new_packet (\
			 0, 0, 0, 0, 0, 0 \
    );
  packet_length = get_size (sent);

/* open the socket */
  if ((open_socket (&socket_descr)) != 0)
    return -1;

/* generate the remote address descriptor */
  if ((generate_address (&address, input_data->host)) != 0)
    {
      close (socket_descr);
      return -1;
    }

/* prepares the file descriptor set for "select" */
  FD_ZERO (&read_set);
  FD_SET (socket_descr, &read_set);

/* ==========================================================
   Get the current logical clock time and fill the xmt field 
   in the message with it. 
   This is the BEGINning of the time critical region.
   ========================================================== */
  get_log_clock (LC, &(attempt_summary->t0), (struct timeval *) NULL, NOW);
  set_xmttime (&(attempt_summary->t0), sent);
  if (sendto (socket_descr, ((char *) sent), packet_length, 0, \
	      (struct sockaddr *) &address, \
	      sizeof (address)) < 0)
    {
      close (socket_descr);
      return -1;
    }
  do
    {
/* =========================================================
   Prepares the timeout for "select". It is better to redo
   this each time. 
   ========================================================= */
      get_deadline (&read_timeout, &(input_data->w_tv), \
		    &(attempt_summary->baseline));
/* =========================================================
   The select unblocks either when a reply arrives (but this
   could be an un-ordered reply), or when the timeout expires.
   If a timeout occurs, the function returns with -1, otherwise
   a read from the socket is performed to receive the pending
   message.
   ========================================================= */
      if (select (FD_SETSIZE, &read_set, NULL, NULL, &read_timeout) == 0)
	{
	  close (socket_descr);
	  return 1;		/* if timeout, return 1 */
	}
      if (read (socket_descr, (char *) received, packet_length) < 0)
	{
	  close (socket_descr);
	  return -1;		/* read failed */
	}
/* ==========================================================
   The current value of the logical clock is taken as the t_1
   time in the protocol (see paper).
   ========================================================== */
      get_log_clock (LC, &(attempt_summary->t1), \
		     (struct timeval *) NULL, NOW);
/* ==========================================================
   The receive system time is also recorded in the attempt_summary
   structure.
   ========================================================== */
      gettimeofday (&(attempt_summary->receive), (struct timezone *) NULL);
/* ==========================================================
   This is the END of the time critical region. If the host
   spends more time in this region than the network to deliver
   the message, than the time spent in this region becomes the
   roundtrip delay. 
   The timestamp of the message is checked to discover disordered
   messages.
   ========================================================== */
      if ((msg_ok_flag = check_timestamp (received, sent)) == 0)
	if (input_data->verbose)
	  printf ("Bad message (out of sequencing)\n");
    }
/* ==========================================================
   And continue until the right message is received, if not
   interrupted by a timeout.
   ========================================================== */
  while (!msg_ok_flag);
  close (socket_descr);

/* Convert the encoded dispersion (or accuracy) into a timeval */
  get_dispersion (&(attempt_summary->server_dispersion), received);
/* Convert the NTP times into timeval */
  get_rectime (&(attempt_summary->tr), received);
  get_xmttime (&(attempt_summary->tt), received);
/* ===========================================================
   Returns with success
   =========================================================== */
  return 0;
}

int
open_socket (int *s)
{
/* Create socket descriptor */
  if ((*s = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    return -1;
  return 0;
}

int
generate_address (struct sockaddr_in *sa, char *host)
{
  struct hostent *d;

/* 
   Fills basic fields of address 
   (from "The GNU C Library" p.164) 
 */
  sa->sin_family = AF_INET;
  sa->sin_port = htons (NTP_PORT);
/* 
   Converts a symbolic name to Internet address 
 */
  if ((d = gethostbyname (host)) != NULL) 
    memcpy(&(sa->sin_addr), d->h_addr_list[0], d->h_length);
  else
    return -1;
  return 0;
}
