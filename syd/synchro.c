/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: synchro.c,v $
   Revision 3.3  1997/12/03 17:34:02  augusto
   Update to use the interface file

   Revision 3.2  1997/11/27 08:17:38  augusto
   w_tv in input_data deleted

   Revision 3.1  1997/11/20 17:46:35  augusto
   This is the first version correctly running on the Internet
   Tested locally using test1 through test10 and on the
   Internet with NTP server tempo.cstv.to.cnr.it

 */

/* 
   todo
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>		/* for macro SIGALRM */
#include <sys/time.h>		/* for struct timeval */
/* These are Syd related headers */
#include <ntp.h>
#include <logck.h>
#include <timeout.h>
#include <tvops.h>
#include "syd.h"
#include "log.h"

int
synchro ()
{
  struct timeval delta_0_tv;	/* delta_0 as a timeval */
  double delta_0,w;		/* delta_0 as a double */
  struct timeval dna_tv;	/* delay to next attempt as a timeval */
  double dna;			/* delay to next attempt */
  struct timeval tmp_tv;	/* useless result */
  char buffer[100];		/* log report buffer */

/* initialize the synchronization mode */
  attempt_summary->mode = NOT_SYNC;
/* ===========================================
   This is the external loop that performs the
   periodic clock synchronization attempts
   =========================================== */
  do
    {
      if (attempt () != 0)
	return -1;
/* Record the new logical clock in the interface file */
      write_log_clock(LC,input_data->ifc_file);
/* Compute the dna */
      get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW);
      _TVTODB (delta_0_tv, delta_0)
      _TVTODB (input_data->w_tv, w)
	dna = (((input_data->delta_max - delta_0) / input_data->rho) - \
	       input_data->k * w);
      _DBTOTV (dna, dna_tv)
	stamp_log ();
      sprintf (buffer, "Delay to next attempt (dna) is %d\' %d\" (%.3f secs)\n",
	       ((int) dna / 60), (((int) dna % 60)), dna);
      write_log (buffer);
      flush_log ();
/* 
   If the "best effort" is chosen, then there might be not even the
   time to wait for the entire dna: in that case the program proceeds
   without sleeping. 
 */
      set_timeout (&(dna_tv), &(attempt_summary->receive));
      wait_timeout ();
    }
  while (1);
}
