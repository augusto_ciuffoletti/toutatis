/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: attempt.c,v $
   Revision 3.2  1998/01/16 13:18:45  augusto
   Terminology: precision is changed into accuracy.

   Revision 3.1  1997/11/20 17:46:35  augusto
   This is the first version correctly running on the Internet
   Tested locally using test1 through test10 and on the
   Internet with NTP server tempo.cstv.to.cnr.it

 */

#include <stdio.h>
#include <signal.h>		/* for signal sets */
#include <sys/time.h>		/* for gettimeofday */
#include <unistd.h>
/* the following are local headers */
#include <ntp.h>
#include <logck.h>
#include <timeout.h>
#include <tvops.h>
#include "log.h"
#include "syd.h"

static void fill_attempt_report (char *b);

/* =======================================================================

   This function performs repeated retries in order to reach contact with
   the server.

   The desired operation consists in obtaining a clock more accurate than a
   given threshold (epsilon). If this result is attained, the function
   returns a 0 value.

   The function also returns 0 if, after a limited number of attempts (k),
   the accuracy is not below epsilon, but is more accurate than a worst case
   limit (delta_max). In this case, the global variable serrno is set to
   BEST_EFFORT (instead of SUCCESS).

   The function returns 1 if the function failed to reach contact with the
   server after a limited number of retries, given that the clock was not in
   contact when the function was called.

   The function returns -1 in case of fatal errors:

   - if the contact was lost during the operation: in this case the serror
   variable is set to CONTACT_LOST;

   - in case of networking problems: in this case the herrno and errno are
   set accordingly.

   The function fills-in the report if and only if it does not terminate
   with a fatal error.

   ======================================================================= */

int
attempt ()
{

  static int is_committed = 0;	/* persistent indication that the
				   clock is delivered as accurate */
  struct timeval tmp_tv;	/* a useless result... */
  struct timeval delta_0_tv;	/* the final clock accuracy */
  double delta_0 = 0.0;		/* like the previous, but double */
  int is_accurate = 0;		/* is the clock accuracy acceptable? */
  char buffer[2000];		/* the retry report buffer */

  stamp_log ();
  write_log ("  New attempt\n");
  flush_log ();
/* reset the serrno global variable */
  if (serrno < 3)
    serrno = SUCCESS;
  else
    {
      serrno = LATENT_SYDE;
      return -1;
    }
  attempt_summary->count = 0;	/* initialize the retry count */
  cancel_timeout ();		/* don't wait before first (re)try */
  do
    {
      wait_timeout ();
/* A new retry begins here */
      (attempt_summary->count)++;
/* Setup the retry baseline */
      gettimeofday (&(attempt_summary->baseline), \
		    (struct timezone *) NULL);
/* 
   Apply the remote clock reading operation. 
   This operation uses a timeout. If that timeout did not expire, 
   and a new retry is required, then the computation must wait
   until it expires. If the retry returns a negative value, the 
   attempt must fail.
 */
      if (retry () < 0)
	{
	  flush_log ();
	  return -1;		/* Fatal error */
	}
/* 
   A new deadline is set only if there is time left
 */
      set_timeout (&(input_data->w_tv), &(attempt_summary->baseline));
/* 
   Exiting the following "if", if the delta_0 has an acceptable value 
   the is_accurate boolean is 1. Otherwise the is_accurate boolean is 0. 
 */
      if (get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW) == 0)
	{
	  _TVTODB (delta_0_tv, delta_0);
	  is_accurate = (delta_0 < input_data->delta_max);
	}
      else
	is_accurate = 0;
/* Check whether contact was lost */
      if ((is_accurate == 0) && (is_committed == 1))
	{
	  serrno = CONTACT_LOST;
	  return -1;		/* fatal error: contact lost after commit */
	}
    }
  while (!((attempt_summary->count == input_data->k) ||
	   ((is_accurate == 1) && (delta_0 < input_data->epsilon))
	 )
    );
/*
   The computation reaches this point if:
   - the clock was not committed after "count" (initial) retries or
   - the accuracy is below the desidered threshold
   and the clock is accurate (note that the delta_0
   is initialized at 0, when also is_accurate is 0) or
   - the clock was not below the desired threshold
   after the "count" attempts, but the accuracy was still acceptable.
   The value of is_accurate discriminates two exit conditions: 
   one that describes an initial failure to reach contact, and another
   that describes a regular operation (even if the final accuracy
   is not the desired one but is just a "best effort").
 */
  cancel_timeout ();
/* Computing the exit conditions */
  if (is_accurate != 1)
    {
      serrno = NO_CONTACT;
      return 1;
    }
  is_committed = 1;		/* now the clock value is committed */
  if (delta_0 > input_data->epsilon)
    serrno = BEST_EFFORT;
  else
    serrno = SUCCESS;
/* Filling the report... */
  stamp_log ();
  fill_attempt_report (buffer);
  write_log (buffer);
  flush_log ();
  return 0;
}

void
fill_attempt_report (char *b)
{
  struct timeval tmp_tv;
  struct timeval delta_0_tv;

  b[0] = '\0';
  sprintf (b + strlen (b), "  Final result was ");
  switch (serrno)
    {
    case SUCCESS:
      sprintf (b + strlen (b), "reached contact, ");
      get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW);
      sprintf (b + strlen (b), "with accuracy delta_0 = %6.2f msecs\n",
	       (delta_0_tv.tv_usec / 1000.0) + (delta_0_tv.tv_sec * 1000));
      sprintf (b + strlen (b), "  Attempt was successful after %d retries\n",
	       attempt_summary->count);

      break;
    case BEST_EFFORT:
      sprintf (b + strlen (b), "best effort, ");
      get_log_clock (LC, &tmp_tv, &delta_0_tv, NOW);
      sprintf (b + strlen (b), "with accuracy delta_0 = %6.2f msecs\n",
	       (delta_0_tv.tv_usec / 1000.0) + (delta_0_tv.tv_sec * 1000));
      break;
    case NO_CONTACT:
      sprintf (b + strlen (b), "no contact.\n");
      break;
    }
  sprintf (b + strlen (b), "\n");
  return;
}
