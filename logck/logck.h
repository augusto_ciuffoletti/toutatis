/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: logck.h,v $
   Revision 2.3  1997/12/03 18:54:36  augusto
   Leap second field and functions added
   Owner pid field and function added
   Stamp functions and update added

   Revision 2.2  1997/12/03 17:25:45  augusto
   The log_clock structure is hidden
   The drift is computed
   The interface file is introduced in this version

   Revision 2.1  1997/11/20 18:45:02  augusto
   First revision tested with Syd v. 3.1

   Revision 1.0  1997/11/13 08:55:36  augusto
   Initial revision

   Revision 1.7  1997/02/26 13:26:42  augusto
   Copyright notice aggiunta

   Revision 1.6  1997/02/20 16:10:51  augusto
   Aggiunta dichiarazione get_deadline

   Revision 1.5  1997/02/13 18:02:38  augusto
   Modificate le costanti utilizzate nelle "return": OK=0, ERROR=-1.
   Aggiunte le funzioni sull'orologio di sistema.

   Revision 1.4  1997/02/11 14:38:07  augusto
   Inclusione dello header di math.h (per la modf).

   Revision 1.3  1997/02/11 08:36:13  augusto
   Inserimento del Log nelle revisioni

 */

#include <math.h>		/* for function modf */
#include <unistd.h>
#include <sys/types.h>

struct log_clock *new_log_clock (double rho);
int is_accurate (struct log_clock *lc);
int get_log_clock (struct log_clock *t, struct timeval *Tx, \
		   struct timeval *precision, struct timeval *when);
int set_log_clock (struct log_clock *LC, struct timeval *Tx, \
		   struct timeval *precision, struct timeval *when);
void set_leap_second(struct log_clock *LC);
void reset_leap_second(struct log_clock *LC);
int read_log_clock (struct log_clock *LC, char *filename);
int write_log_clock (struct log_clock *LC, char *filename);
char *log_clock2c (struct log_clock *LC);
int get_offset (struct log_clock *LC, struct timeval *offset);
int get_drift (struct log_clock *LC, int *drift);
pid_t get_owner (struct log_clock *LC);
unsigned int get_stamp (struct log_clock *LC);
int get_leap_second(struct log_clock *LC);
