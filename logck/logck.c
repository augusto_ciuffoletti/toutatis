/*
   syd - synchronizes the local system clock with a remote NTP server
   Copyright (C) 1997  Augusto Ciuffoletti

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Contact address: augusto@di.unipi.it
 */

/*
   $Log: logck.c,v $
   Revision 2.4  1998/03/12 08:28:39  augusto
   Leap second pending notice added to log_clock2c
   Cosmetics to log_clock2c

   Revision 2.3  1997/12/03 18:54:36  augusto
   Leap second field and functions added
   Owner pid field and function added
   Stamp functions and update added

   Revision 2.2  1997/12/03 17:25:45  augusto
   The log_clock structure is hidden
   The drift is computed
   The interface file is introduced in this version

   Revision 2.1  1997/11/20 18:45:02  augusto
   First revision tested with Syd v. 3.1

   Revision 1.0  1997/11/13 08:55:36  augusto
   Initial revision

   Revision 1.9  1997/03/24 14:15:46  augusto
   Aggiunto test sulla "set" e "get" _deadline. Testato parzialmente.

   Revision 1.8  1997/03/24 09:18:45  augusto
   Aggiungo la is_accurate

   Revision 1.7  1997/02/26 13:26:42  augusto
   Copyright notice aggiunta

   Revision 1.6  1997/02/20 14:08:49  augusto
   Correzione di una svista in new_log_clock.
   Realizzazione di get_deadline

   Revision 1.5  1997/02/13 18:02:38  augusto
   Modificate le costanti utilizzate nelle "return": OK=0, ERROR=-1.
   Aggiunte le funzioni sull'orologio di sistema.

   Revision 1.4  1997/02/11 14:38:07  augusto
   Inclusione dello header di math.h (per la modf).

   Revision 1.3  1997/02/11 08:35:22  augusto
   Inserimento del log delle revisioni

 */

/* 
   This package implements the abstraction of the logical clock.

   The logical clock is implemented on the Unix system clock, which in its turn
   uses the hardware (C-Mos) clock.

   The logical clock has a resolution of the micro-second, and has an interface
   that is similar to the interface of the system clock (the timeval), but with
   some important differences:

   - it can dynamically change its speed or jump forward or backward;

   - there may be several instances of the logical clock;

   - each logical clock has a "precision" and an "accuracy" associated with it:
   the first one represents an upper bound of the offset between the clock
   value and the reference time (for instance UTC);

   - some operations may be "displaced" in time, giving the result with respect
   to a time different from the present time;

 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <tvops.h>
#include <fcntl.h>
#include "logck.h"

struct log_clock
  {
    pid_t owner;		/* the pid of the process that last
				   updated the interface file */
    unsigned int stamp;		/* incremented each time the log_clock
				   is written to the interface file */
    int is_valid;		/* if 1 the clock precision is bound */
    struct timeval origin;	/* the time when the clock was set up */
    struct timeval offset;	/* the offset between the system clock
				   and the log_clock when the log_clock 
				   was set up */
    struct timeval precision;	/* the upper bound of the difference between
				   the log_clock and the time reference (e.g.
				   UTC) when the clock was set up */
    long int drift;		/* the apparent speed with which the log_clock
                                   runs apart from the system clock, in usecs
                                   per second (i.e. parts per million). The
                                   drift estimate is updated only if the
                                   new estimate has an acceptable precision */
    double drift_precision;	/* the precision of the drift estimate */
    double rho;			/* the upper bound of the drift of the logical
				   clock with respect to the reference time */
    int leap_second;		/* presence of the leap second today */
  };

struct flock lck;		/* the lock used for mutual exclusion on read
				   and writes on the interface file */

struct log_clock *
new_log_clock (double rho)
{
  struct log_clock *lc;

  /* initialize the lock file */
  lck.l_whence = 0;		/* lock extends to the whole file */
  lck.l_start = 0;
  lck.l_len = 0;
  lck.l_pid = getpid ();
  /* create a new log_clock */
  lc = (struct log_clock *) malloc (sizeof (struct log_clock));
  /* the following is "best effort": the logical clock is
     initialized with the present time, but is declared as 
     not valid. Other fields are null */
  gettimeofday (&lc->origin, (struct timezone *) NULL);
  lc->rho = rho;
  _RESETTV (lc->offset);
  _RESETTV (lc->precision);
  lc->drift=0;
  lc->is_valid=0;
  lc->owner = getpid();
  lc->stamp=0;
  return lc;
}

int 
is_accurate (struct log_clock *lc)
{
  if (lc->is_valid == 1)
    return 1;
  else
    return 0;
}

int
get_log_clock (struct log_clock *LC, struct timeval *Tx, \
	       struct timeval *precision, struct timeval *when)
{
  struct timeval tmp_tv;
  double tmp_db, prec_db;

  if (when == (struct timeval *) NULL)
    gettimeofday (&tmp_tv, (struct timezone *) NULL);
  else
    _COPYTV ((*when), tmp_tv);
  _DIFFTV (tmp_tv, (LC->origin));
  if ((LC->is_valid) && (precision != NULL))
    {
      /* computing the precision */
      _TVTODB (tmp_tv, tmp_db);
      _TVTODB ((LC->precision), prec_db);
      prec_db += (tmp_db * (double) (LC->rho)) / 1000000;
      _DBTOTV (prec_db, (*precision));
    }
  /* tmp_tv = tmp_tv * drift */
  _ADDTV (tmp_tv, (LC->offset));
  _ADDTV (tmp_tv, (LC->origin));
  _COPYTV (tmp_tv, (*Tx));
  if (LC->is_valid)
    return 0;
  else
    return -1;
}

int
set_log_clock (struct log_clock *LC, struct timeval *Tx, \
	       struct timeval *precision, struct timeval *when)
{
  struct timeval tmp_tv, old_offset_tv, old_origin_tv, old_precision_tv;
  double delta_offset, delta_origin, delta_precision, drift_precision;

/* computing the new origin */
  if (when == (struct timeval *) NULL)
    gettimeofday (&tmp_tv, (struct timezone *) NULL);
  else
    _COPYTV ((*when), tmp_tv);
/* recording the old values */
  _COPYTV ((LC->offset), old_offset_tv);
  _COPYTV ((LC->origin), old_origin_tv);
  _COPYTV ((LC->precision), old_precision_tv);
/* computing new offset */
  _COPYTV ((*Tx), (LC->offset));
  _DIFFTV ((LC->offset), tmp_tv);
/* entering new time origin */
  _COPYTV (tmp_tv, (LC->origin));
/* entering precision */
  _COPYTV ((*precision), (LC->precision));
  if ((LC->is_valid) == 1)
    {
/* computing new drift */
      _DIFFTV (old_offset_tv, (LC->offset));
      _DIFFTV (old_origin_tv, (LC->origin));
      _DIFFTV (old_precision_tv, (LC->precision));
      _TVTODB (old_offset_tv, delta_offset);
      _TVTODB (old_origin_tv, delta_origin);
      _TVTODB (old_precision_tv, delta_precision);
      drift_precision = (2 * delta_precision) / delta_offset;
      /* update drift only if its precision is < 100% */
      if ( drift_precision < 1 )
        {
           LC->drift = (long) ((delta_offset * 1000000.0) / delta_origin); 
           LC->drift_precision = drift_precision;
	}
    }
/* setting logical clock as valid */
  LC->is_valid = 1;
/* updating the owner */
  LC->owner = getpid();
/* increment the stamp */
  (LC->stamp)++;
  return 0;
}

/* The following read specific fields of the logck structure */

int
get_offset (struct log_clock *LC, struct timeval *offset)
{
  if ((LC->is_valid) == 1)
    {
      _COPYTV ((LC->offset), (*offset));
      return 0;
    }
  else
    return -1;
}

int
get_drift (struct log_clock *LC, int *drift)
{
  if ((LC->is_valid) == 1)
    {
      *drift = LC->drift;
      return 0;
    }
  else
    return -1;
}

pid_t
get_owner (struct log_clock *LC)
{
    return (LC->owner);
}

unsigned int
get_stamp (struct log_clock *LC)
{
    return (LC->stamp);
}

int
read_log_clock (struct log_clock *LC, char *filename)
{
  int fd;

  /* initializing the read lock (do not write while I am reading) */
  lck.l_type = F_RDLCK;
  if ((fd = open (filename, O_RDONLY)) == -1)
    {
      perror ("Opening the logical clock file");
      return -1;
    };
  fcntl (fd, F_SETLKW, &lck);	/* lock before reading */
  if (read (fd, LC, sizeof (struct log_clock)) == -1)
      return -1;
  close (fd);			/* close and remove the lock */
  return 0;
}

int
write_log_clock (struct log_clock *LC, char *filename)
{
  int fd;

/* initializing the write lock (no one else can access the file) */
  lck.l_type = F_WRLCK;
  if ((fd = open (filename,
		  O_WRONLY | O_CREAT,
		  S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP)) == -1)
    {
      perror ("Opening the logical clock file");
      return -1;
    };
  fcntl (fd, F_SETLKW, &lck);	/* lock before writing */
  LC->stamp++; /* increment the timestamp */
  if (write (fd, LC, sizeof (struct log_clock)) == -1)
    {
      perror ("Writing the logical clock file");
      return -1;
    };
  close (fd);			/* close and remove the lock */
  return 0;
}

char *
log_clock2c (struct log_clock *LC)
{
  static char buffer[2000];

  if (LC == NULL)
    return NULL;
  if (LC->is_valid == 0)
    {
      sprintf (buffer, "The clock has not yet been set up\n");
      return buffer;
    }
  sprintf (buffer,
	   "Last set up was at %s.\n",
	   ctimeval (&(LC->origin)));
  sprintf (buffer + strlen (buffer),
	   "Offset at last set up was %d secs and %d usecs.\n",
	   (LC->offset).tv_sec, (LC->offset).tv_usec);
  sprintf (buffer + strlen (buffer),
       "The precision when the clock was set up was %d secs and %d usecs.\n",
	   (LC->precision).tv_sec, (LC->precision).tv_usec);
  sprintf (buffer + strlen (buffer),
	   "The last computed drift is %ld ppms.\n",
	   LC->drift);
  sprintf (buffer + strlen (buffer),
	   "The absolute value of the drift must be below %d ppms.\n",
	   (int) (LC->rho));
  sprintf (buffer + strlen (buffer),
	   "Owner of this logical clock is process %u, and the stamp is %u.\n",
	   (unsigned int) (LC->owner), (LC->stamp));
  if (LC->leap_second)
    sprintf (buffer + strlen (buffer), "Leap second IS pending!\n");
  else
    sprintf (buffer + strlen (buffer), "Leap second is not pending.\n");
  return buffer;
}

void
set_leap_second(struct log_clock *LC)
{
  LC->leap_second = 1;
  return;
}

void
reset_leap_second(struct log_clock *LC)
{
  LC->leap_second = 0;
  return;
}

int
get_leap_second(struct log_clock *LC)
{
if ((LC->is_valid) == 1)
  return LC->leap_second;
else
  return -1;
}
